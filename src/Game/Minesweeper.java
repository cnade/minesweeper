package Game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.swing.Timer;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

public class Minesweeper extends JFrame implements MouseListener {

	private static final long serialVersionUID = 1L;
	public int row, size, numberOfMines, numberOfMinesLeft;
	int time = 0;
	int correctFlags;
	public Timer timer;
	public JButton startButton = new JButton();
	public JButton scoreButton = new JButton();
	ArrayList<Integer> mines = new ArrayList<Integer>();
	ArrayList<Integer> places = new ArrayList<Integer>();
	ArrayList<Integer> numbers = new ArrayList<Integer>();
	ArrayList<Integer> list = new ArrayList<Integer>();
	ArrayList<Integer> checked = new ArrayList<Integer>();
	ArrayList<Integer> nullid = new ArrayList<Integer>();
	ArrayList<String> scorelist = new ArrayList<String>();
	



	ImageIcon mineIc = new ImageIcon(this.getClass().getResource("/mine.gif"));
	ImageIcon sandIc = new ImageIcon(this.getClass().getResource("/sand.gif"));
	ImageIcon sand2Ic = new ImageIcon(this.getClass().getResource("/sand2.gif"));
	ImageIcon flagIc = new ImageIcon(this.getClass().getResource("/flag.gif"));

	public boolean gameOver = false;
	public boolean gameWon = false;
	public boolean gameLost = false;
	boolean started = false;
	public static boolean restart = true;
	JFrame frame = new JFrame();
	JButton[] button = new JButton[900];
	String a1 = "btn1";
	String a0 = "btn0";
	JLabel minesleft = new JLabel();
	JLabel timepassed = new JLabel();

	public final ButtonGroup buttonGroup = new ButtonGroup();
	public JTextField textField;
	public JTextField textField_1;

	public Minesweeper(int restart_NOF, int restart_row, int restart_size, Dimension restart_dimension) {
		if (restart == false)  //juhul, kui m�ng alustatakse algusest (ehk ei tehta restarti), tuleb minna settingute men��sse
			createView_menu();
		if (restart) {  //kui m�ngija otsustab samade settingutega teha restart, j�etakse settingute men�� vahele ja v�etakse vajalikud parameetrid eelmisest m�ngust
			frame.setTitle("Minesweeper");
			frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
			frame.setResizable(false);
			frame.setSize(new Dimension(350, 350));
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);
			row = restart_row;
			size = restart_size;
			numberOfMines = restart_NOF;
			numberOfMinesLeft = restart_NOF;
			frame.setSize(restart_dimension);
			grid();
			frame.setLocationRelativeTo(null);
			arrayplaces(row, places);
			createView();
		}
	}

	private void createView_menu() {

		ButtonGroup buttonGroup = new ButtonGroup();
		JTextField csize;
		JTextField cmines;

		frame.setResizable(false);
		frame.setTitle("Minesweeper");
		frame.setSize(350, 350);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		JPanel menu = new JPanel();
		menu.setBounds(0, 0, 344, 325);
		frame.getContentPane().add(menu);
		menu.setLayout(null); //layout style null v�imaldab iga nupu, labeli etc asukohta ja suurust muuta kergesti piksli t�psusega

		JRadioButton easy = new JRadioButton("Easy       (9x9, 10 mines)");
		easy.setSelected(true);
		buttonGroup.add(easy);
		easy.setFont(new Font("Gadugi", Font.BOLD, 14));
		easy.setForeground(new Color(255, 255, 255));
		easy.setOpaque(false);
		easy.setBounds(41, 99, 300, 23);
		menu.add(easy);

		JRadioButton hard = new JRadioButton("Hard      (16x16, 35 mines)");
		buttonGroup.add(hard);
		hard.setOpaque(false);
		hard.setFont(new Font("Gadugi", Font.BOLD, 14));
		hard.setForeground(new Color(255, 255, 255));
		hard.setBounds(41, 125, 300, 23);
		menu.add(hard);

		JRadioButton expert = new JRadioButton("Expert    (20x20, 45 mines)");
		buttonGroup.add(expert);
		expert.setOpaque(false);
		expert.setFont(new Font("Gadugi", Font.BOLD, 14));
		expert.setForeground(new Color(255, 255, 255));
		expert.setBounds(41, 151, 300, 23);
		menu.add(expert);

		JRadioButton custom = new JRadioButton("Custom");
		buttonGroup.add(custom);
		custom.setFont(new Font("Gadugi", Font.BOLD, 14));
		custom.setForeground(new Color(255, 255, 255));
		custom.setOpaque(false);
		custom.setBounds(41, 177, 200, 23);
		menu.add(custom);
		//kui m�ngija tahab, saab ise valida m�ngulaua suuruse ja miinide arvu
		csize = new JTextField();
		csize.setBounds(120, 207, 75, 20);
		menu.add(csize);
		csize.setColumns(10);

		cmines = new JTextField();
		cmines.setBounds(120, 238, 75, 20);
		menu.add(cmines);
		cmines.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("Row  size:");
		lblNewLabel_1.setFont(new Font("Gadugi", Font.BOLD, 14));
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setBounds(38, 213, 140, 14);
		menu.add(lblNewLabel_1);

		JLabel lblMines = new JLabel("Mines:");
		lblMines.setForeground(Color.WHITE);
		lblMines.setFont(new Font("Gadugi", Font.BOLD, 14));
		lblMines.setBounds(68, 244, 100, 14);
		menu.add(lblMines);

		JButton startButton = new JButton("START");
		startButton.setBounds(200, 211, 100, 47);
		menu.add(startButton);

		JButton scoreButton = new JButton("View scores");
		scoreButton.setBounds(115, 285, 120, 20);
		menu.add(scoreButton);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(this.getClass().getResource("/grass.png")));
		lblNewLabel.setBounds(0, 0, 370, 370);
		menu.add(lblNewLabel);
		//kui m�ngija tahab high score vaadata, kutsutakse showscore funktsioon
		scoreButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					showscores();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean integr = true;
				if (easy.isSelected()) {
					row = 9;
					numberOfMines = 10;
					numberOfMinesLeft = 10;
					frame.setSize(new Dimension(450, 500));
				}

				if (hard.isSelected()) {
					row = 15;
					numberOfMines = 35;
					numberOfMinesLeft = 35;
					frame.setSize(new Dimension(720, 770));
				}

				if (expert.isSelected()) {
					row = 20;
					numberOfMines = 60;
					numberOfMinesLeft = 60;
					frame.setSize(new Dimension(945, 995));
				}

				if (custom.isSelected()) {
					integr = isInteger(csize.getText());
					if (integr)
						integr = isInteger(cmines.getText());
					if (integr && Integer.valueOf(csize.getText())>=5) {
						row = Integer.valueOf(csize.getText());
						numberOfMines = Integer.valueOf(cmines.getText());
						numberOfMinesLeft = Integer.valueOf(cmines.getText());
						int x = (row * 45) + 45;
						frame.setSize(x, x + 45);
						
					} else {
						integr = false;
						JFrame error = new JFrame();
						error.setVisible(true);
						error.setTitle("Error");
						error.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
						error.setResizable(false);
						error.setSize(new Dimension(230, 100));
						JPanel as = new JPanel();
						error.getContentPane().add(as);
						JLabel label = new JLabel("Enter correct numeric values!");
						JLabel label2 = new JLabel("Row size must be atleast 5");
						as.add(label);
						as.add(label2);
						JButton ok = new JButton("Ok");
						as.add(ok);
						ok.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								error.dispose();
							}
						});
					}
				}
				if (integr) {
					size = row * row;
					menu.setVisible(false);
					grid();
					arrayplaces(row, places);
					frame.setLocationRelativeTo(null);
					createView();
				}
			}
		});
	}

	public void showscores() throws IOException {

		File scorefile = new File("scores.txt");
		//kui faili ei eksisteeri, loob selle
		if (scorefile.createNewFile()) {
			System.out.println("File is created!");
		} else {
			System.out.println("File already exists.");
		}
		//teeb highscore akna
		JFrame scores = new JFrame();
		scores.setVisible(true);
		scores.setTitle("Highscores");
		scores.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		scores.setResizable(false);
		scores.setSize(new Dimension(350, 350));
		JPanel asd = new JPanel();
		scores.getContentPane().add(asd);
		asd.setLayout(null);
		//kutsub funktsiooni, mis loeb failist skoorid ja lisab need arraylisti
		readscores();
		int i = 0;
		int y = 55;
		JLabel labels1 = new JLabel();
		JLabel[] labels = new JLabel[50];
		labels1.setText("Time    Name");
		labels1.setFont(new Font("Gadugi", Font.BOLD, 15));
		labels1.setBounds(120, 40, 250, 25);
		asd.add(labels1);
		//kuvab skoorid arraylistist
		for (String line : scorelist) {
			labels[i] = new JLabel();
			labels[i].setText(line);
			labels[i].setFont(new Font("Arial", Font.BOLD, 13));
			labels[i].setBounds(125, y, 100, 25);
			asd.add(labels[i]);
			y += 15;
		}
	}
	//kui m�ngija valis settingute men��st custom suuruse ja miinidega m�ngulaua, kontrollib, et oleks ikka numbrid sisestatud suurusteks
	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}
		return true;
	}
	//loeb failist skoorid ja lisab arraylisti
	public ArrayList<String> readscores() {
		File file = new File("scores.txt");
		BufferedReader reader = null;

		try {
			reader = new BufferedReader(new FileReader(file));
			String text = null;

			while ((text = reader.readLine()) != null) {
				scorelist.add(text);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {
			}
		}
		scorelist.sort(null);
		return scorelist;

	}
	//loob m�ngulaua
	private void createView() {

		JPanel pf = new JPanel(new GridBagLayout());
		pf.setBorder(new EmptyBorder(10, 10, 10, 10));
		pf.setBackground(Color.GRAY.darker().darker());
		frame.getContentPane().add(pf);
		GridBagConstraints c = new GridBagConstraints(); //tekitab n� ruudustiku
		c.gridx = 0;
		c.gridy = 0;
		int j = 0;

		for (int i = 0; i < size; i++) {
			button[i] = new JButton();
			if (mines.get(i) == 1) {
				button[i].setName("mine");
			}
			if (mines.get(i) == 0) {
				button[i].setName("safe");
			}
			button[i].setPreferredSize(new Dimension(45, 45));
			button[i].putClientProperty("index", i);

			if (j == row) { // kui �hesolevate nuppude arv v�rdub m�ngulaua rea suurusega, liigub rea v�rra allapoole
				j = 0;
				c.gridy++;
				c.gridx = 0;
			}
			button[i].setIcon(sandIc);
			button[i].setFont(new Font("Arial", Font.BOLD, 15));
			button[i].setHorizontalTextPosition(SwingConstants.CENTER);

			pf.add(button[i], c);
			button[i].addMouseListener(this);
			c.gridx++;
			j++;

		}
		c.gridx = 0;
		c.gridy += 2;
		minesleft = new JLabel();
		minesleft.setFont(new Font("Arial", Font.BOLD, 17));
		minesleft.setForeground(Color.red);
		timepassed = new JLabel();
		timepassed.setForeground(Color.white);
		timepassed.setFont(new Font("Arial", Font.BOLD, 17));
		updatelabel(); //uuendab timerit

		c.gridwidth = 4;
		c.fill = GridBagConstraints.HORIZONTAL;

		pf.add(minesleft, c);
		c.gridx = row-2;
		timepassed.setText("Time: 0");
		pf.add(timepassed, c);
		c.ipadx = 0;

		pf.setVisible(true);
		arrayplaces2();
	}

	private int check2(int index, ArrayList<Integer> array) {
		int tmp = 0;
		int sum = 0;
		for (int place : array) {
			tmp = place + index;
			if (tmp < 0 || tmp > size - 1)
				continue;
			if (index == 0 && (tmp == row - 1) || (index == row - 1 && (tmp == 0 || tmp == row)))
				continue;
			if (index == size - 1 && (tmp == size - row)
					|| (index == size - row && (tmp == size - 1 || tmp == size - row - 1)))
				continue;
			if ((index % row == 0) && (tmp == index - 1 || tmp == index + row - 1 || tmp == index - row - 1))
				continue;
			if (((index - row + 1) % row == 0)
					&& (tmp == index + 1 || tmp == index - row + 1 || tmp == index + row + 1))
				continue;
			if (((((JButton) button[tmp]).getName())).equals("mine"))
				sum++;
		}
		return sum;
	}
	//lisab arraylisti ruudu indexid, mida vajutusel kontrollida
	private void addlist(int index) {
		list.add(index);
		nullid.add(index);
		int tmp = 0;
		while (list.isEmpty() == false) {
			here: for (int place : places) {
				tmp = place + list.get(0);
				for (int check : checked) {
					if (tmp == check)
						continue here;
				}
				if (tmp < 0 || tmp > size - 1)
					continue;
				if (list.get(0) == 0 && (tmp == row - 1) || (list.get(0) == row - 1 && (tmp == 0 || tmp == row)))
					continue;
				if (list.get(0) == size - 1 && (tmp == size - row)
						|| (list.get(0) == size - row && (tmp == size - 1 || tmp == size - row - 1)))
					continue;
				if ((list.get(0) % row == 0)
						&& (tmp == list.get(0) - 1 || tmp == list.get(0) + row - 1 || tmp == list.get(0) - row - 1))
					continue;
				if (((list.get(0) - row + 1) % row == 0)
						&& (tmp == list.get(0) + 1 || tmp == list.get(0) - row + 1 || tmp == list.get(0) + row + 1))
					continue;
				if (numbers.get(tmp) == 0) {
					if (button[tmp].getIcon().equals(flagIc) == false)
						button[tmp].setIcon(sand2Ic);
					list.add(tmp);
					nullid.add(tmp);
				}

			}
			boolean a = false;
			for (int check : checked) {
				if (check == list.get(0))
					a = true;
			}
			if (a == false)
				checked.add(list.get(0)); //lisab kontrollitud koha listi, et seda enam ei kontrollitaks. optimeerib mingil m��ral m�ngu t��d
			list.remove(0);
		}
		Set<Integer> temp = new HashSet<>();
		temp.addAll(nullid);
		nullid.clear();
		nullid.addAll(temp);

		for (int element : nullid) {
			for (int place : places) {
				tmp = place + element;
				if (tmp < 0 || tmp > size - 1)
					continue;
				if (element == 0 && (tmp == row - 1) || (element == row - 1 && (tmp == 0 || tmp == row)))
					continue;
				if (element == size - 1 && (tmp == size - row)
						|| (element == size - row && (tmp == size - 1 || tmp == size - row - 1)))
					continue;
				if ((element % row == 0)
						&& (tmp == element - 1 || tmp == element + row - 1 || tmp == element - row - 1))
					continue;
				if (((element - row + 1) % row == 0)
						&& (tmp == element + 1 || tmp == element - row + 1 || tmp == element + row + 1))
					continue;
				if (button[tmp].getIcon().equals(flagIc) == false)
					button[tmp].setIcon(sand2Ic);
				if (numbers.get(tmp) != 0 && button[tmp].getIcon().equals(flagIc) == false) {
					if (numbers.get(tmp) == 1)
						button[tmp].setForeground(Color.cyan.darker());
					if (numbers.get(tmp) == 2)
						button[tmp].setForeground(Color.green);
					if (numbers.get(tmp) == 3)
						button[tmp].setForeground(Color.yellow);
					if (numbers.get(tmp) == 4)
						button[tmp].setForeground(Color.orange);
					if (numbers.get(tmp) == 5)
						button[tmp].setForeground(Color.red);
					if (numbers.get(tmp) == 6)
						button[tmp].setForeground(Color.red.darker());
					if (numbers.get(tmp) == 7)
						button[tmp].setForeground(Color.MAGENTA);
					if (numbers.get(tmp) == 8)
						button[tmp].setForeground(Color.pink);
					button[tmp].setText(Integer.toString(numbers.get(tmp)));
				}

			}
		}
		nullid.clear();
	}

	private void check(int index) {
		if (((((JButton) button[index]).getName())).equals("mine")) {
			button[index].setIcon(mineIc);
			button[index].setText("");
			gameOver = true;
			gameLost = true;
			ko();
			return;
		}

		int sum = 0;
		if (((((JButton) button[index]).getName())).equals("safe")) {
			button[index].setIcon(sand2Ic);
			sum = numbers.get(index);
			if (sum == 0) {
				addlist(index);
			}

		}

		if (((((JButton) button[index]).getName())).equals("safe") && (button[index].getIcon().equals(sand2Ic))
				&& sum != 0) {
			if (sum == 1)
				button[index].setForeground(Color.cyan.darker());
			if (sum == 2)
				button[index].setForeground(Color.green);
			if (sum == 3)
				button[index].setForeground(Color.yellow);
			if (sum == 4)
				button[index].setForeground(Color.orange);
			if (sum == 5)
				button[index].setForeground(Color.red);
			if (sum == 6)
				button[index].setForeground(Color.red.darker());
			if (sum == 7)
				button[index].setForeground(Color.MAGENTA);
			if (sum == 8)
				button[index].setForeground(Color.pink);

			button[index].setText(Integer.toString(sum));
		}
		ko();
	}

	private void arrayplaces2() {
		int tmp = 0;

		for (int i = 0; i < size; i++) {
			tmp = check2(i, places);
			numbers.add(tmp);
		}

	}

	private void arrayplaces(int i, ArrayList<Integer> array) {

		array.add(-i - 1);
		array.add(-i);
		array.add(-i + 1);
		array.add(-1);
		array.add(1);
		array.add(i - 1);
		array.add(i);
		array.add(i + 1);

	}

	public void Timer() {
		timer = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				time++;
				timepassed.setText("Time: " + String.valueOf(time));
			}
		});
		timer.start();
	}

	private void grid() {

		for (int i = 0; i < size; i++) {
			if (i < numberOfMines)
				mines.add(1);
			else
				mines.add(0);
		}
		Collections.shuffle(mines);
	}

	public void updatelabel() {
		minesleft.setText("Mines left: " + String.valueOf(numberOfMinesLeft));
	}

	private void ko() {
		if (gameOver) {
			timer.stop();
			scorewindow();
		}
	}
	
	public void write(String line) throws IOException {
		File file = new File("scores.txt");
		//FileWriter fos = new FileWriter(file);
	 
		BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));
			bw.write(line);
			bw.newLine();
		bw.close();
	}

	private void scorewindow() {
		JFrame scorewindow = new JFrame();
		JLabel tekst = new JLabel();
		if (gameLost) {
			scorewindow.setTitle("Game over");
			tekst = new JLabel("RIP");
			tekst.setFont(new Font("Arial", Font.BOLD, 20));
			tekst.setBounds(150, 40, 150, 30);
		}

		else {
			scorewindow.setTitle("Mines cleared!");
			tekst = new JLabel("Gz, you won");
			tekst.setFont(new Font("Arial", Font.BOLD, 20));
			tekst.setBounds(108, 40, 150, 30);
		}

		scorewindow.setDefaultCloseOperation(EXIT_ON_CLOSE);
		scorewindow.setResizable(false);
		scorewindow.setSize(new Dimension(350, 350));
		scorewindow.setLocationRelativeTo(null);
		scorewindow.setVisible(true);
		JPanel sw = new JPanel();
		sw.setLayout(null);
		scorewindow.getContentPane().add(sw);
		JLabel finaltime = new JLabel();
		finaltime.setText("Your time: " + String.valueOf(time));
		finaltime.setBounds(130, 100, 100, 40);
		finaltime.setFont(new Font("Arial", Font.BOLD, 13));
		sw.add(finaltime);
		if (row == 9 && numberOfMines == 10 && gameWon == true) {
			JLabel addscore = new JLabel("Enter your name: ");
			addscore.setFont(new Font("Arial", Font.BOLD, 14));
			addscore.setBounds(45, 150, 155, 25);
			JTextField name = new JTextField();
			name.setBounds(185, 150, 110, 25);
			sw.add(addscore);
			sw.add(name);
			JButton submit = new JButton("Submit");
			submit.setBounds(171, 180, 124, 25);
			JButton vscores = new JButton("View scores");
			vscores.setBounds(45, 180, 124, 25);
			sw.add(submit);
			sw.add(vscores);
			vscores.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
				try {
					showscores();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				}
			});
			
		
			submit.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
				    try {
						write(time +"         "+ name.getText());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					
				}
			});
		}
		sw.add(tekst);
		JButton newgame = new JButton("New game");
		newgame.setBounds(45, 220, 250, 30);
		JButton changesettings = new JButton("Settings");
		changesettings.setBounds(45, 255, 134, 30);
		JButton exitgame = new JButton("Exit");
		exitgame.setBounds(181, 255, 114, 30);
		changesettings.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				frame.dispose();
				scorewindow.dispose();
				main(null);
			}
		});
		exitgame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);

			}
		});
		newgame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				restart = true;
				Dimension restart_dimension = frame.getSize();
				frame.setVisible(false);
				frame.dispose();
				scorewindow.dispose();
				mines.clear();
				numbers.clear();
				places.clear();
				list.clear();
				checked.clear();
				nullid.clear();
				new Minesweeper(numberOfMines, row, size, restart_dimension);
			}
		});
		sw.add(changesettings);
		sw.add(exitgame);
		sw.add(newgame);
	}

	public static void main(String args[]) {
		restart = false;

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				int tmp1 = 0;

				Minesweeper window = new Minesweeper(tmp1, tmp1, tmp1, new Dimension(0, 0));
				window.frame.setVisible(true);
			}

		});
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		int index = (Integer) (((JComponent) e.getSource()).getClientProperty("index"));
		if (started == false) {
			started = true;
			Timer();
		}
		if (gameOver == false) {
			if (SwingUtilities.isRightMouseButton(e)) {

				if (button[index].getIcon().equals(flagIc)) {
					button[index].setIcon(sandIc);

					if (button[index].getName().equals("mine"))
						correctFlags--;
					if (button[index].getName().equals("mine") == false)
						correctFlags++;
					if (correctFlags == numberOfMines) {
						gameOver = true;
						gameWon = true;
						ko();
						timer.stop();
					}
					numberOfMinesLeft++;
					updatelabel();
				}

				else if (button[index].getIcon().equals(sandIc)) {
					button[index].setIcon(flagIc);

					if (button[index].getName().equals("mine"))
						correctFlags++;
					if (button[index].getName().equals("mine") == false)
						correctFlags--;
					if (correctFlags == numberOfMines) {
						gameOver = true;
						gameWon = true;
						ko();
						timer.stop();
					}
					numberOfMinesLeft--;
					updatelabel();
				}
			}

			if (SwingUtilities.isLeftMouseButton(e)) {
				if ((button[index].getIcon().equals(sandIc))) {
					check(index);
				}
			}
		}

	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent arg0) {

	}

	@Override
	public void mousePressed(MouseEvent arg0) {

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {

	}
}
